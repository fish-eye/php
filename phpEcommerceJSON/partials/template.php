<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>

	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/united/bootstrap.css">

</head>
<body>
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="../index.php">BatanGadgets</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		<div class="collapse navbar-collapse" id="navbarColor01">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href="../views/catalog.php">Gadgets<span class="sr-only">(current)</span></a>
		      </li>
		      <?php
		      	session_start();
		      	if(isset($_SESSION['email']) && $_SESSION['email']=="admin@batangadgets.com"){
		      ?>
		      	<li class="nav-item">
		        	<a class="nav-link" href="add-item.php">Add Item</a>
		      	</li>
		      <?php		
		      	}else{
		      ?>
		      	<li class="nav-item">
		        	<a class="nav-link" href="cart.php">Cart<span class="badge bg-info">
		        		<?php  
		        			if(isset($_SESSION['cart'])){
		        				echo array_sum($_SESSION['cart']);
		        			}else{
		        				echo 0;
		        			}
		        		?>
		        	</span></a>
		    	</li>
		      <?php		
		      	}
		      ?>
		    </ul>
		    <ul class="navbar-nav ml-auto mx-3">
		   	<?php  
		      	if(isset($_SESSION['firstName'])){
		    ?>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Hello <?php echo $_SESSION['firstName'] ?>!</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="../controllers/logout-process.php">Logout</a>
		      </li>
		    <?php		
		      	}else{
		     ?>
		      <li class="nav-item">
		        <a class="nav-link" href="login.php">Login</a>
		      </li>
		       <li class="nav-item">
		        <a class="nav-link" href="register.php">Register</a>
		      </li>
		    <?php
		      	}
		     ?>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="text" placeholder="Search">
		      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
		    </form>
		</div>
	</nav>

	<!-- Page Contents -->
	<?php get_body_contents()?>

	<!-- Footer -->
	<footer class="page-footer font-small text-white bg-primary">
		<div class="footer-copyright text-center py-3">
			© 2020 by BatanGadgets
		</div>
	</footer>
</body>
</html>