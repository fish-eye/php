<?php  
	require "../partials/template.php";

	function get_body_contents(){
?>
	<h1 class="text-center py-3">Cart</h1>
	<hr>
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<div class="">
				<table class="table table-striped">
					<thead>
						<th class="text-center">Item Name:</th>
						<th class="text-center">Item Price:</th>
						<th class="text-center">Item Quantity:</th>
						<th class="text-center">Subtotal</th>
						<th></th>
					</thead>
					<tbody>
						<?php  
							$items = file_get_contents("../assets/lib/products.json");
							$items_array = json_decode($items, true);

							$total = 0;

							if(isset($_SESSION['cart'])){
								foreach ($_SESSION['cart'] as $name => $quantity){
									foreach ($items_array as $indiv_item) {
										if($name == $indiv_item['name']){
											$subtotal = $indiv_item['price']*$quantity;
											$total += $subtotal;
											?>
										<tr>
											<td><?php echo $name ?></td>
											<td><?php echo number_format($indiv_item['price'], 2, ".", ",") ?></td>
											<td>
												<form action="../controllers/add-to-cart-process.php" method="POST">
													<div class="input-group">
														<input type="hidden" name="name" value="<?php echo $name ?>">
														<input type="hidden" name="fromCartPage" value="fromCartPage">
														<input type="number" name="quantity" value="<?php echo $quantity ?>" class="form-control text-center">
														<div class="input-group-append">
															<button class="btn btn-sm btn-success" type="submit">Update</button>	
														</div>
													</div>
												</form>
											</td>
											<!-- <td><?php echo $quantity ?></td> -->
											<td><?php echo number_format($subtotal, 2, ".", ",") ?></td>
											<td>
												<a href="../controllers/remove-from-cart-process.php?name=<?php echo $name ?>" class="btn btn-danger">Remove from cart</a>
											</td>
										</tr>
											<?php
										}
									}
								}
							}
						?>
						<tr class="bg-info">
							<td></td>
							<td></td>
							<td class="text-right">Total: </td>
							<td><?php echo number_format($total, 2, ".", ",") ?></td>
							<td>
								<a href="../controllers/empty-cart-process.php?name=<?php echo $name ?>"" class="btn btn-danger">Empty cart</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php		
	}
?>