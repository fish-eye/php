// fetch("http://jsonplaceholder.typicode.com/users")
// 	.then (response_from_fetch =>{
// 		return response_from_fetch.json();
// 	})
// .then(data_from_response1=>{
// 	users = data_from_response1;
// })

fetch("https://api.nasa.gov/planetary/apod?api_key=ILEDlbbwq5JVE0PlCcYBgSBPkPvcCu8vhoDiEizN")
.then(res=>res.json())
.then(res=>{
	document.getElementById('nasaPhoto').src = res.url;
	document.getElementById('nasaPhoto').nextElementSibling.innerText = res.title;
	document.getElementById('nasaPhoto').nextElementSibling.nextElementSibling.innerText = res.date;
	document.getElementById('nasaPhoto').nextElementSibling.nextElementSibling.nextElementSibling.innerText = res.explanation;
})